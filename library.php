<?php

function printSafe($string) {
    if (is_array($string) || is_object($string)) {
        print("<pre align=left>");
        print_r($string);
        print("</pre>");
        return;
    }

    print($string);
}